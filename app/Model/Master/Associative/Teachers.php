<?php
/**
 * Contains code written by the PT Qomteq Maju Bersama.
 * Any other use of this code is in violation of copy rights.
 *
 * @package   Matalogix
 * @author    Valerius Iman <valerius@mbteknologi.com>
 * @copyright 2021 PT Qomteq Maju Bersama.
 */

namespace App\Model\Viewer\Master\Associative;

use App\Frame\Formatter\SqlHelper;
use App\Frame\Formatter\StringFormatter;
use App\Frame\Gui\Table;
use App\Frame\Mvc\AbstractViewerModel;
use App\Model\Dao\Master\Associative\TeacherEducationsDao;
use App\Model\Dao\Master\Associative\TeachersDao;
use App\Frame\Gui\FieldSet;
use App\Frame\Gui\Portlet;
use App\Frame\Formatter\Trans;

/**
 * Class to handle the creation of detail Teachers page
 *
 * @package    app
 * @subpackage Model\Viewer\Master\Associative
 * @author     Valerius Iman <valerius@mbteknologi.com>
 * @copyright  2021 PT Qomteq Maju Bersama.
 */
class Teachers extends AbstractViewerModel
{
    /**
     * Constructor to load when there is a new instance created.
     *
     * @param array $parameters To store the parameter from http.
     */
    public function __construct(array $parameters)
    {
        # Call parent construct.
        parent::__construct(get_class($this), 'tch', 'tch_id');
        $this->setParameters($parameters);
    }

    /**
     * Function to do the update of the transaction.;
     *
     * @return void
     */
    protected function doUpdate(): void
    {

    }

    /**
     * Abstract function to load the data.
     *
     * @return array
     */
    public function loadData(): array
    {
//        dd(TeachersDao::getByReferenceAndSystem($this->getDetailReferenceValue(), $this->User->getSsId()));
//        dd(TeachersDao::getByReference($this->getDetailReferenceValue()));
        return TeachersDao::getByReferenceAndSystem($this->getDetailReferenceValue(), $this->User->getSsId());
    }

    /**
     * Abstract function to load form of the page.
     *
     * @return void
     */
    public function loadForm(): void
    {
        $this->Tab->addPortlet('general', $this->getGeneralPortlet());
        $this->Tab->addPortlet('general', $this->getPositionPortlet());
        $this->Tab->addPortlet('education', $this->getEducationFieldSet());
    }

    /**
     * Function to load the validation role.
     *
     * @return void
     */
    public function loadValidationRole(): void
    {
        if ($this->getFormAction() === null) {

        } else {
            parent::loadValidationRole();
        }
    }

    /**
     * Function to get the general Field Set.
     *
     * @return Portlet
     */
    private function getGeneralPortlet(): Portlet
    {
        # Instantiate Portlet Object
        $portlet = new Portlet('TchPtl', Trans::getWord('personalIdentity'));
        $portlet->setGridDimension(6, 6, 12);

        $gender = 'Male';
        if ($this->getStringParameter('tch_sex') !== 'M') {
            $gender = 'Female';
        }

        $data = [
            [
                'label' => Trans::getWord('name'),
                'value' => $this->getStringParameter('tch_name')
            ],
            [
                'label' => Trans::getWord('employeeNumber'),
                'value' => $this->getStringParameter('tch_employee_number')
            ],
            [
                'label' => Trans::getWord('pob'),
                'value' => $this->getStringParameter('tch_pob')
            ],
            [
                'label' => Trans::getWord('dob'),
                'value' => $this->getStringParameter('tch_dob')
            ],
            [
                'label' => Trans::getWord('gender'),
                'value' => $gender
            ],
        ];

        $content = StringFormatter::generateCustomTableView($data);
        $portlet->addText($content);

        return $portlet;
    }

    /**
     * Function to get the general Field Set.
     *
     * @return Portlet
     */
    private function getPositionPortlet(): Portlet
    {
        # Instantiate Portlet Object
        $portlet = new Portlet('positionPtl', Trans::getWord('position'));
        $portlet->setGridDimension(6, 6, 12);

        $data = [
            [
                'label' => Trans::getWord('position'),
                'value' => $this->getStringParameter('tch_position')
            ],
            [
                'label' => Trans::getWord('employeeStatus'),
                'value' => $this->getStringParameter('tch_employee_status')
            ],
            [
                'label' => Trans::getWord('category'),
                'value' => $this->getStringParameter('tch_category')
            ],
            [
                'label' => Trans::getWord('startingDate'),
                'value' => $this->getStringParameter('tch_starting_date')
            ],
            [
                'label' => Trans::getWord('fieldOfStudy'),
                'value' => $this->getStringParameter('tch_field_of_study')
            ],
        ];

        $content = StringFormatter::generateCustomTableView($data);
        $portlet->addText($content);

        return $portlet;
    }

    /**
     * Function to get the education Field Set.
     *
     * @return Portlet
     */
    private function getEducationFieldSet(): Portlet
    {

        $table = new Table('TeTbl');
        $table->setHeaderRow([
            'te_title' => Trans::getWord('title'),
            'te_institution' => Trans::getWord('institution'),
            'te_graduation_year' => Trans::getWord('graduationYear'),
            'te_major' => Trans::getWord('major'),
        ]);
        $helper = new SqlHelper();
        $helper->addStringWhere('te_tch_id', $this->getDetailReferenceValue());
        $data = TeacherEducationsDao::loadData($helper);
        $table->addRows($data);
        # Create a portlet box.
        $portlet = new Portlet('educationPtl', Trans::getWord('educations'));
        $portlet->addTable($table);

        return $portlet;
    }
}
