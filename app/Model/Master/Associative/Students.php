<?php
/**
 * Contains code written by the PT Qomteq Maju Bersama.
 * Any other use of this code is in violation of copy rights.
 *
 * @package   Matalogix
 * @author    Valerius Iman <valerius@mbteknologi.com>
 * @copyright 2021 PT Qomteq Maju Bersama.
 */

namespace App\Model\Viewer\Master\Associative;

use App\Frame\Formatter\StringFormatter;
use App\Frame\Mvc\AbstractViewerModel;
use App\Model\Dao\Master\Associative\StudentsDao;
use App\Frame\Gui\FieldSet;
use App\Frame\Gui\Portlet;
use App\Frame\Formatter\Trans;

/**
 * Class to handle the creation of detail Students page
 *
 * @package    app
 * @subpackage Model\Viewer\Master\Associative
 * @author     Valerius Iman <valerius@mbteknologi.com>
 * @copyright  2021 PT Qomteq Maju Bersama.
 */
class Students extends AbstractViewerModel
{
    /**
     * Constructor to load when there is a new instance created.
     *
     * @param array $parameters To store the parameter from http.
     */
    public function __construct(array $parameters)
    {
        # Call parent construct.
        parent::__construct(get_class($this), 'std', 'std_id');
        $this->setParameters($parameters);
    }

    /**
     * Function to do the update of the transaction.;
     *
     * @return void
     */
    protected function doUpdate(): void
    {

    }

    /**
     * Abstract function to load the data.
     *
     * @return array
     */
    public function loadData(): array
    {
        return StudentsDao::getByReferenceAndSystem($this->getDetailReferenceValue(), $this->User->getSsId());
    }

    /**
     * Abstract function to load form of the page.
     *
     * @return void
     */
    public function loadForm(): void
    {
        $this->Tab->addPortlet('general', $this->getGeneralPortlet());
        $this->Tab->addPortlet('general', $this->getPersonalPortlet());
        $this->Tab->addPortlet('general', $this->getPhysicalPortlet());
        $this->Tab->addPortlet('general', $this->getAddressPortlet());
        $this->Tab->addPortlet('father', $this->getFatherPortlet());
        $this->Tab->addPortlet('mother', $this->getMotherPortlet());
        $this->Tab->addPortlet('guardian', $this->getGuardianPortlet());
    }

    /**
     * Function to load the validation role.
     *
     * @return void
     */
    public function loadValidationRole(): void
    {
        if ($this->getFormAction() === null) {

        } else {
            parent::loadValidationRole();
        }
    }

    /**
     * Function to get the general Field Set.
     *
     * @return Portlet
     */
    private function getGeneralPortlet(): Portlet
    {
        # Instantiate Portlet Object
        $portlet = new Portlet('studentsPtl', Trans::getWord('personalIdentity'));
        $portlet->setGridDimension(6, 6, 12);

        $gender = 'Male';
        if ($this->getStringParameter('std_sex') !== 'M') {
            $gender = 'Female';
        }

        $data = [
            [
                'label' => Trans::getWord('fullName'),
                'value' => $this->getStringParameter('std_full_name')
            ],
            [
                'label' => Trans::getWord('nickName'),
                'value' => $this->getStringParameter('std_nick_name')
            ],
            [
                'label' => Trans::getWord('schoolIdNumber'),
                'value' => $this->getStringParameter('std_school_id_number')
            ],
            [
                'label' => Trans::getWord('nationalIdNumber'),
                'value' => $this->getStringParameter('std_national_id_number')
            ],
            [
                'label' => Trans::getWord('residenceIdNumber'),
                'value' => $this->getStringParameter('std_residence_id_number')
            ],
            [
                'label' => Trans::getWord('phone'),
                'value' => $this->getStringParameter('std_phone_number')
            ],
            [
                'label' => Trans::getWord('email'),
                'value' => $this->getStringParameter('std_email')
            ],
            [
                'label' => Trans::getWord('pob'),
                'value' => $this->getStringParameter('std_pob')
            ],
            [
                'label' => Trans::getWord('dob'),
                'value' => $this->getStringParameter('std_dob')
            ],
            [
                'label' => Trans::getWord('gender'),
                'value' => $gender
            ],
        ];

        $content = StringFormatter::generateCustomTableView($data);
        $portlet->addText($content);

        return $portlet;
    }


    /**
     * Function to get the general Field Set.
     *
     * @return Portlet
     */
    private function getPersonalPortlet(): Portlet
    {
        # Instantiate Portlet Object
        $portlet = new Portlet('personalPtl', Trans::getWord('personalIdentity'));
        $portlet->setGridDimension(6, 6, 12);

        $data = [

            [
                'label' => Trans::getWord('religion'),
                'value' => $this->getStringParameter('std_religion')
            ],
            [
                'label' => Trans::getWord('citizen'),
                'value' => $this->getStringParameter('std_citizen')
            ],
            [
                'label' => Trans::getWord('childNumberInFamily'),
                'value' => $this->getStringParameter('std_child_number_in_family')
            ],
            [
                'label' => Trans::getWord('numberOfSiblings'),
                'value' => $this->getStringParameter('std_number_of_siblings')
            ],
            [
                'label' => Trans::getWord('numberOfStepSiblings'),
                'value' => $this->getStringParameter('std_number_of_step_siblings')
            ],
            [
                'label' => Trans::getWord('numberOfAdoptiveSiblings'),
                'value' => $this->getStringParameter('std_number_of_adoptive_siblings')
            ],
            [
                'label' => Trans::getWord('everyDayLanguage'),
                'value' => $this->getStringParameter('std_every_day_language')
            ],
        ];

        $content = StringFormatter::generateCustomTableView($data);
        $portlet->addText($content);

        return $portlet;
    }

    /**
     * Function to get the general Field Set.
     *
     * @return Portlet
     */
    private function getPhysicalPortlet(): Portlet
    {
        # Instantiate Portlet Object
        $portlet = new Portlet('PhysicalPtl', Trans::getWord('physical'));
        $portlet->setGridDimension(6, 6, 12);

        $data = [
            [
                'label' => Trans::getWord('weight'),
                'value' => $this->getStringParameter('std_weight')
            ],
            [
                'label' => Trans::getWord('height'),
                'value' => $this->getStringParameter('std_height')
            ],
            [
                'label' => Trans::getWord('bloodType'),
                'value' => $this->getStringParameter('std_blood_type')
            ],
            [
                'label' => Trans::getWord('diseaseBeenSuffered'),
                'value' => $this->getStringParameter('std_disease_been_suffered')
            ],
        ];

        $content = StringFormatter::generateCustomTableView($data);
        $portlet->addText($content);

        return $portlet;
    }

    /**
     * Function to get the general Field Set.
     *
     * @return Portlet
     */
    private function getAddressPortlet(): Portlet
    {
        # Instantiate Portlet Object
        $portlet = new Portlet('addressPtl', Trans::getWord('address'));
        $portlet->setGridDimension(6, 6, 12);

        $data = [
            [
                'label' => Trans::getWord('country'),
                'value' => $this->getStringParameter('std_country')
            ],
            [
                'label' => Trans::getWord('state'),
                'value' => $this->getStringParameter('std_state')
            ],
            [
                'label' => Trans::getWord('city'),
                'value' => $this->getStringParameter('std_city')
            ],
            [
                'label' => Trans::getWord('district'),
                'value' => $this->getStringParameter('std_district')
            ],
            [
                'label' => Trans::getWord('address'),
                'value' => $this->getStringParameter('std_address')
            ],
            [
                'label' => Trans::getWord('postalCode'),
                'value' => $this->getStringParameter('std_postal_code')
            ],
        ];

        $content = StringFormatter::generateCustomTableView($data);
        $portlet->addText($content);

        return $portlet;
    }


    /**
     * Function to get the general Field Set.
     *
     * @return Portlet
     */
    private function getFatherPortlet(): Portlet
    {
        # Instantiate Portlet Object
        $portlet = new Portlet('fatherPtl', Trans::getWord('father'));
        $portlet->setGridDimension(6, 6, 12);

        $data = [
            [
                'label' => Trans::getWord('residenceIdNumber'),
                'value' => $this->getStringParameter('std_father_residence_id_number')
            ],
            [
                'label' => Trans::getWord('fullName'),
                'value' => $this->getStringParameter('std_father_full_name')
            ],
            [
                'label' => Trans::getWord('pob'),
                'value' => $this->getStringParameter('std_father_pob')
            ],
            [
                'label' => Trans::getWord('dob'),
                'value' => $this->getStringParameter('std_father_dob')
            ],
            [
                'label' => Trans::getWord('religion'),
                'value' => $this->getStringParameter('std_father_religion')
            ],
            [
                'label' => Trans::getWord('lastEducation'),
                'value' => $this->getStringParameter('std_father_last_education')
            ],
            [
                'label' => Trans::getWord('profession'),
                'value' => $this->getStringParameter('std_father_profession')
            ],
            [
                'label' => Trans::getWord('averageMonthlyIncome'),
                'value' => $this->getStringParameter('std_father_average_monthly_income')
            ],
            [
                'label' => Trans::getWord('address'),
                'value' => $this->getStringParameter('std_father_address')
            ],
        ];

        $content = StringFormatter::generateCustomTableView($data);
        $portlet->addText($content);

        return $portlet;
    }

    /**
     * Function to get the general Field Set.
     *
     * @return Portlet
     */
    private function getMotherPortlet(): Portlet
    {
        # Instantiate Portlet Object
        $portlet = new Portlet('motherPtl', Trans::getWord('mother'));
        $portlet->setGridDimension(6, 6, 12);

        $data = [
            [
                'label' => Trans::getWord('residenceIdNumber'),
                'value' => $this->getStringParameter('std_mother_residence_id_number')
            ],
            [
                'label' => Trans::getWord('fullName'),
                'value' => $this->getStringParameter('std_mother_full_name')
            ],
            [
                'label' => Trans::getWord('pob'),
                'value' => $this->getStringParameter('std_mother_pob')
            ],
            [
                'label' => Trans::getWord('dob'),
                'value' => $this->getStringParameter('std_mother_dob')
            ],
            [
                'label' => Trans::getWord('religion'),
                'value' => $this->getStringParameter('std_mother_religion')
            ],
            [
                'label' => Trans::getWord('lastEducation'),
                'value' => $this->getStringParameter('std_mother_last_education')
            ],
            [
                'label' => Trans::getWord('profession'),
                'value' => $this->getStringParameter('std_mother_profession')
            ],
            [
                'label' => Trans::getWord('averageMonthlyIncome'),
                'value' => $this->getStringParameter('std_mother_average_monthly_income')
            ],
            [
                'label' => Trans::getWord('address'),
                'value' => $this->getStringParameter('std_mother_address')
            ],
        ];

        $content = StringFormatter::generateCustomTableView($data);
        $portlet->addText($content);

        return $portlet;
    }

    /**
     * Function to get the general Field Set.
     *
     * @return Portlet
     */
    private function getGuardianPortlet(): Portlet
    {
        # Instantiate Portlet Object
        $portlet = new Portlet('guardianPtl', Trans::getWord('guardian'));
        $portlet->setGridDimension(6, 6, 12);

        $data = [
            [
                'label' => Trans::getWord('residenceIdNumber'),
                'value' => $this->getStringParameter('std_guardian_residence_id_number')
            ],
            [
                'label' => Trans::getWord('fullName'),
                'value' => $this->getStringParameter('std_guardian_full_name')
            ],
            [
                'label' => Trans::getWord('pob'),
                'value' => $this->getStringParameter('std_guardian_pob')
            ],
            [
                'label' => Trans::getWord('dob'),
                'value' => $this->getStringParameter('std_guardian_dob')
            ],
            [
                'label' => Trans::getWord('religion'),
                'value' => $this->getStringParameter('std_guardian_religion')
            ],
            [
                'label' => Trans::getWord('lastEducation'),
                'value' => $this->getStringParameter('std_guardian_last_education')
            ],
            [
                'label' => Trans::getWord('profession'),
                'value' => $this->getStringParameter('std_guardian_profession')
            ],
            [
                'label' => Trans::getWord('averageMonthlyIncome'),
                'value' => $this->getStringParameter('std_guardian_average_monthly_income')
            ],
            [
                'label' => Trans::getWord('address'),
                'value' => $this->getStringParameter('std_guardian_address')
            ],
        ];

        $content = StringFormatter::generateCustomTableView($data);
        $portlet->addText($content);

        return $portlet;
    }
}
