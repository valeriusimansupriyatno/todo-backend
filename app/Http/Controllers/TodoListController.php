<?php

namespace App\Http\Controllers;

use App\Model\TodoListModel;
use Illuminate\Http\JsonResponse;

class TodoListController extends Controller
{

    /**
     * function to get all data
     *
     * @return JsonResponse
     */
    public function getData(): JsonResponse
    {
        $data = TodoListModel::loadData();
        $total = TodoListModel::loadTotalData();

        return response()->json([
            'success' => true,
            'total' => $total,
            'data' => $data
        ], 200);
    }

    /**
     * function to get data active
     *
     * @return JsonResponse
     */
    public function getDataActive(): JsonResponse
    {
        $wheres[] = "active = 'Y'";
        $data = TodoListModel::loadData($wheres);
        $total = TodoListModel::loadTotalData($wheres);

        return response()->json([
            'success' => true,
            'total' => $total,
            'data' => $data
        ], 200);
    }

    /**
     * function to get data complete
     *
     * @return JsonResponse
     */
    public function getDataComplete(): JsonResponse
    {
        $wheres[] = "complete = 'Y'";
        $data = TodoListModel::loadData($wheres);
        $total = TodoListModel::loadTotalData($wheres);

        return response()->json([
            'success' => true,
            'total' => $total,
            'data' => $data
        ], 200);
    }

    /**
     * function to update all data active
     *
     * @return JsonResponse
     */
    public function updateAllActive(): JsonResponse
    {
        $colVal = [
            'active' => 'Y',
            'complete' => 'N'
        ];
        $clear = new TodoListModel();
        $clear->doClear('Y', $colVal);

        return response()->json(['success' => true,], 200);

    }

    /**
     * function to update all data complete
     *
     * @return JsonResponse
     */
    public function updateAllComplete(): JsonResponse
    {
        $colVal = [
            'active' => 'N',
            'complete' => 'Y'
        ];
        $clear = new TodoListModel();
        $clear->doClear('N', $colVal);

        return response()->json(['success' => true,], 200);
    }

    /**
     * function to update data
     *
     * @return JsonResponse
     */
    public function updateTodo(): JsonResponse
    {
        $id = \request()->id;
        $todo = TodoListModel::getByReference($id);

        if ($todo['active'] === 'N') {
            $colVal = [
                'active' => 'Y',
                'complete' => 'N'
            ];
        } else {
            $colVal = [
                'active' => 'N',
                'complete' => 'Y'
            ];
        }

        $update = new TodoListModel();
        $update->doUpdate($id, $colVal);

        return response()->json(['success' => true,], 200);
    }
}
