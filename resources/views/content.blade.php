@extends('home')
@section('content')
    @foreach($list as $lt)
        <li {!! $lt['complete'] === 'Y' ? 'class="completed"' : '' !!}>
            <div class="view">
                <input id="{{$lt['id']}}" class="toggle check" type="checkbox" name="check" value="{{$lt['id']}}"
                       data-category="{{$lt['id']}}" {!! $lt['complete'] === 'Y' ? 'checked' : '' !!}>
                <label>{{$lt['text']}}</label>
                <button class="destroy"></button>
                <input class="edit" name="{{"id-".$lt['id']}}" value="{{$lt['id']}}">
            </div>
        </li>
    @endforeach
@endsection
@section('footer')
    <span class="todo-count"><strong>{{$total}}</strong> item left</span>
@endsection
