<?php
/**
 * Contains code written by the PT Qomteq Maju Bersama.
 * Any other use of this code is in violation of copy rights.
 *
 * @package   Matalogix
 * @author    Valerius Iman <valerius@mbteknologi.com>
 * @copyright 2022 PT Qomteq Maju Bersama.
 */

namespace App\Model\Viewer\Master\Associative;

use App\Frame\Formatter\SqlHelper;
use App\Frame\Gui\Html\Buttons\Button;
use App\Frame\Gui\Icon;
use App\Frame\Gui\TableDatas;
use App\Frame\Mvc\AbstractViewerModel;
use App\Model\Dao\Master\Associative\ClassesDao;
use App\Frame\Gui\FieldSet;
use App\Frame\Gui\Portlet;
use App\Frame\Formatter\Trans;
use App\Model\Dao\Master\Associative\StudentClassesDao;
use App\Model\Dao\Master\Associative\StudentsDao;

/**
 * Class to handle the creation of detail Classes page
 *
 * @package    app
 * @subpackage Model\Viewer\Master\Associative
 * @author     Valerius Iman <valerius@mbteknologi.com>
 * @copyright  2022 PT Qomteq Maju Bersama.
 */
class Classes extends AbstractViewerModel
{
    /**
     * Constructor to load when there is a new instance created.
     *
     * @param array $parameters To store the parameter from http.
     */
    public function __construct(array $parameters)
    {
        # Call parent construct.
        parent::__construct(get_class($this), 'cls', 'cls_id');
        $this->setParameters($parameters);
    }

    /**
     * Function to do the update of the transaction.;
     *
     * @return void
     */
    protected function doUpdate(): void
    {
        $std_id = $this->getArrayParameter('std_id');
        $proceed = $this->getArrayParameter('proceed');
        if (count($std_id) > 0) {
            $sclDao = new StudentClassesDao();
            foreach ($std_id as $key => $value) {
                if (array_key_exists($key, $proceed) === true && $proceed[$key] === 'Y') {
                    $colValStd = [
                        'scl_ss_id' => $this->User->getSsId(),
                        'scl_cls_id' => $this->getDetailReferenceValue(),
                        'scl_std_id' => $std_id[$key],
                    ];
                    $sclDao->doInsertTransaction($colValStd);
                }
            }
        }
    }

    /**
     * Abstract function to load the data.
     *
     * @return array
     */
    public function loadData(): array
    {
        return ClassesDao::getByReferenceAndSystem($this->getDetailReferenceValue(), $this->User->getSsId());
    }

    /**
     * Abstract function to load form of the page.
     *
     * @return void
     */
    public function loadForm(): void
    {
        $this->Tab->addPortlet('general', $this->getGeneralPortlet());
    }

    /**
     * Function to load the validation role.
     *
     * @return void
     */
    public function loadValidationRole(): void
    {
        if ($this->getFormAction() === null) {

        } else {
            parent::loadValidationRole();
        }
    }

    /**
     * Function to get the general Field Set.
     *
     * @return Portlet
     */
    private function getGeneralPortlet(): Portlet
    {
        # Create a table.
        $pageTable = new TableDatas('stdTbl');
        $pageTable->setHeaderRow([
            'std_id' => '',
            'name' => '',
            'email' => '',
            'phone' => '',
            'std_full_name' => Trans::getWord('name'),
            'std_email' => Trans::getWord('email'),
            'std_phone_number' => Trans::getWord('phone'),
            'proceed' => Trans::getWord('proceed'),
        ]);
        $pageTable->setRowsPerPage(30);
        $helper = new SqlHelper();
        $helper->addStringWhere('std_ss_id', $this->User->getSsId());
        $helper->addNullWhere('scl_id');
        $stDao = StudentsDao::loadDataStudents($helper);
        $index = 0;
        $results = [];
        foreach ($stDao as $row) {
            $row['std_id'] = $this->Field->getHidden('std_id[' . $index . ']', $row['std_id']);
            $row['name'] = $this->Field->getHidden('std_full_name[' . $index . ']', $row['std_full_name']);
            $row['email'] = $this->Field->getHidden('std_email[' . $index . ']', $row['std_email']);
            $row['phone'] = $this->Field->getHidden('std_phone_number[' . $index . ']', $row['std_phone_number']);
            $checked = false;
            if (empty($row['scl_id']) === false) {
                $checked = true;
                $pageTable->addCellAttribute('proceed', $index, 'class', 'bg-green');
            }
            $check = $this->Field->getCheckBox('proceed[' . $index . ']', 'Y', $checked);

            $row['proceed'] = $check;
            $results[] = $row;
            $index++;
        }
        $pageTable->addRows($results);
        # Add special settings to the table
        $pageTable->addColumnAttribute('proceed', 'style', 'text-align: center;');
        # Create a portlet box.
        $portlet = new Portlet('psPtl', Trans::getWord('studentSelection'));
        $portlet->addTable($pageTable);

        return $portlet;
    }

    /**
     * Function to load default button
     *
     * @return void
     */
    protected function loadDefaultButton(): void
    {

        if ($this->isValidParameter('cls_deleted_on') === false) {
            $btnUpdate = new Button('btnUpdate', Trans::getWord('save'), 'button');
            $btnUpdate->setIcon(Icon::Save)->btnSuccess()->pullRight()->btnMedium();
            $btnUpdate->addAttribute('onclick', "App.submitForm('" . $this->getMainFormId() . "')");
            $this->View->addButtonAtTheBeginning($btnUpdate);
        }
        parent::loadDefaultButton();

    }
}
