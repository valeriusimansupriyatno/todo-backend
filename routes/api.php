<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('list','TodoListController@getData');
Route::get('active','TodoListController@getDataActive');
Route::get('complete','TodoListController@getDataComplete');
Route::get('allActive','TodoListController@updateAllActive');
Route::get('allComplete','TodoListController@updateAllComplete');
Route::post('update','TodoListController@updateTodo');
